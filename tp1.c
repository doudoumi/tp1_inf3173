#include<stdio.h>
#include<pthread.h>
#include<stdlib.h>
#include<string.h>
#include<stdbool.h>

// Function Prototypes
int validateCMDArgs(int argc, char **argv);
char *strsep(char **stringp, const char *delim);
int checkValidNumber(int number);
int ***createMatrixFromFile(char *fileName, int numberOfMatrix);
struct MatrixModel *createMatrixModelListFromMatrixList(int ***matrixList, int numberOfMatrix);
bool matrixValidation(int **matrix);
void *T_Collone_Verification(void *matrixModel);
void *T_Ligne_Verification(void *matrixModel);
void checkThreadResults(int threadLineResult, int threadColumnResult);
int countMatrixInFile (char *fileName);
void *printMatrixModelList(void *matrixModelList);
void T_Bloc_Verification(void *matrixModel);

// Struct Models
struct MatrixModel {

	int **table;
	int line;
	int column;

};

// Global Variables
int threadLineResult,threadBlocResult,threadBlocCpt, threadColumnResult = 0;

// Global Constants
const char *msgErr1 = "La taille de la grille de Sudoku devrait être 9x9";
const char *msgSuccess = "Bravo! Votre Sudoku est valide!";

int main(int argc, char **argv) {

	int numberOfMatrix = countMatrixInFile(argv[1]);
	int ***matrixList = malloc(sizeof(int **) * numberOfMatrix);
	matrixList = createMatrixFromFile(argv[1], numberOfMatrix);
	struct MatrixModel *matrixModelList;
	matrixModelList = malloc(sizeof(struct MatrixModel) * numberOfMatrix);
	matrixModelList = createMatrixModelListFromMatrixList(matrixList, numberOfMatrix);


	for (int i = 0; i < numberOfMatrix; ++i) {
		printf("%d - Struct MatrixModel\n", i + 1);
		printf("-----------------------\n");
		printf("Line : %d\n", matrixModelList[i].line);
		printf("Column : %d\n", matrixModelList[i].column);
		for (int j = 0; j < 9; ++j) {
			for (int k = 0; k < 9; ++k) {
				printf("%d ", matrixModelList[i].table[j][k]);
			}
			printf("\n");
		}
		printf("\n\n");
	}
	T_Bloc_Verification(matrixModelList[1].table);

	free(matrixList);
	free(matrixModelList);


	// if (validateCMDArgs(argc, argv) == 1) exit(1);

	// struct MatrixModel *matrixModel;
	// matrixModel = malloc(sizeof(struct MatrixModel));
	// (*matrixModel).table = createMatrixFromFile(argv[1]);
	// (*matrixModel).line = 0;
	// (*matrixModel).column = 0;

	// pthread_t threadId,thread_colonne,thread_ligne;
	// pthread_attr_t threadAttributes;
	// pthread_attr_init(&threadAttributes);
	// pthread_create(&threadId, &threadAttributes, printMatrix, matrixModel);
	// pthread_join(threadId, NULL);
	// pthread_create(&thread_colonne, &threadAttributes, T_Collone_Verification, matrixModel);
	// pthread_join(thread_colonne,NULL);
	// pthread_create(&thread_ligne,&threadAttributes,T_Ligne_Verification,matrixModel);
	// pthread_join(thread_ligne,NULL);
	// free((*matrixModel).table);
	// free(matrixModel);

	// printf("\n");

	// checkThreadResults(threadLineResult, threadColumnResult);

	// printf("\n\n\n");

}

/*
 * Takes a list of 9x9 arrays and number of matrix as arguments and 
 * returns a list of struct MatrixModel.
 */
struct MatrixModel *createMatrixModelListFromMatrixList(int ***matrixList, int numberOfMatrix) {

	struct MatrixModel *matrixModelList;
	matrixModelList = malloc(sizeof(struct MatrixModel) * numberOfMatrix);

	for (int i = 0; i < numberOfMatrix; ++i) {
		struct MatrixModel *matrixModel;
		matrixModel = malloc(sizeof(struct MatrixModel));
		(*matrixModel).table = matrixList[i];
		(*matrixModel).line = 0;
		(*matrixModel).column = 0;
		matrixModelList[i] = (*matrixModel);
		free(matrixModel);
	}

	return matrixModelList;

}

/*
 * Reads the file in the argument and counts the number
 * of new lines. Returns the number of matrix in the file
 * which is the number of new lines found +1.
 */
int countMatrixInFile (char *fileName) {

	FILE* file = fopen(fileName, "r");
	char line[256];
	int newLineCounter = 0;

	while (fgets(line, sizeof(line), file)) {
		if (strcmp(line, "\n") == 0) {
			newLineCounter ++;
		}
	}

	fclose(file);

	return newLineCounter + 1;

}

/**
 * Validates if all enteries of a matrix are between 1 and 9
 **/
bool matrixValidation(int **matrix) {
	bool valide = true;
	for (int i = 0; i < 9; ++i) {
		for (int j = 0; j < 9; ++j) {
			if(matrix[i][j] < 1 || matrix[i][j] > 9){
				valide = false;
				break;
			}
		}
	}
	return valide;
}

/**
 * Validates arguments from the command line.
 * Returns 0 if the arguments are valid.
 * Returns 1 if the arguments are invalid.
 */
int validateCMDArgs(int argc, char **argv) {

	if (argc != 2) {
		fprintf(stderr, "Usage: %s <INPUT FILE.txt> \n", argv[0]);
		return 1;
	}

	return 0;

}



/**
 *  -----------------------
 */
int ***createMatrixFromFile(char *fileName, int numberOfMatrix) {

	FILE* file = fopen(fileName, "r");
	char line[256];
	char *slicedLine;
	char *characterFound;
	int **matrixCreated = malloc(sizeof(int *) * 9);
	int ***matrixList = malloc(sizeof(int **) * numberOfMatrix);
	int matrixRow = 0;
	int i = 0;

	while (fgets(line, sizeof(line), file)) {
		if (strcmp(line, "\n")) {
			slicedLine = line;
			matrixCreated[matrixRow] = malloc(sizeof(int) * 9);
			int matrixColumn = 0;
			while((characterFound = strsep(&slicedLine, " ")) != NULL) {
				matrixCreated[matrixRow][matrixColumn] = atoi(characterFound);
				matrixColumn++;
			}
			matrixRow++;
			if (matrixRow == 9) {
				matrixList[i] = malloc(sizeof(matrixCreated));
				matrixList[i] = matrixCreated;
				matrixCreated = malloc(sizeof(int **) * 9);
				i++;
				matrixRow = 0;
			}
		}
	}

	fclose(file);

	return matrixList;

}

/*
 * Vérifie la validité des colonnes de notre plateaux de jeu, cette fonction est manipulé par un thread.
 *
 * @param matrixModel  une matrice 9*9 de int qui est notre plateaux de jeu.
 */
void *T_Collone_Verification(void *matrixModel){

	struct MatrixModel *matrix = (struct MatrixModel*)matrixModel;

	for(int i=0;i < 9 ; ++i){
		bool colonne_check[9] ={false,false,false,false,false,false,false,false,false};
		for(int j=0;j< 9 ; ++j){
			switch(matrix->table[j][i]){
				case 1:
					colonne_check[0]= true;
					break;
				case 2:
					colonne_check[1]= true;
					break;
				case 3:
					colonne_check[2]= true;
					break;
				case 4:
					colonne_check[3]= true;
					break;
				case 5:
					colonne_check[4]= true;
					break;
				case 6:
					colonne_check[5]= true;
					break;
				case 7:
					colonne_check[6]= true;
					break;
				case 8:
					colonne_check[7]= true;
					break;
				case 9:
					colonne_check[8]= true;
					break;
			}
		}
		for(int k=0;k< 9;++k){
			if(colonne_check[k] != true){
				threadColumnResult = 1;
				break;
			}
		}
	}
	pthread_exit(0);
}
/*
 *Vérifie la validité des lignes de notre plateaux de jeu, cette fonction est manipulé par un thread.
 *
 *@param matrixModel  une matrice 9*9 de int qui est notre plateaux de jeu.
 */
void *T_Ligne_Verification(void *matrixModel){

	struct MatrixModel *matrix = (struct MatrixModel*)matrixModel;

	for(int i=0;i < 9 ; ++i){
		bool ligne_check[9] ={false,false,false,false,false,false,false,false,false};
		for(int j=0;j< 9 ; ++j){
			switch(matrix->table[i][j]){
				case 1:
					ligne_check[0]= true;
					break;
				case 2:
					ligne_check[1]= true;
					break;
				case 3:
					ligne_check[2]= true;
					break;
				case 4:
					ligne_check[3]= true;
					break;
				case 5:
					ligne_check[4]= true;
					break;
				case 6:
					ligne_check[5]= true;
					break;
				case 7:
					ligne_check[6]= true;
					break;
				case 8:
					ligne_check[7]= true;
					break;
				case 9:
					ligne_check[8]= true;
					break;
			}
		}
		for(int k=0;k< 9;++k){
			if(ligne_check[k] != true){
				threadLineResult = 1;
				break;
			}    
		}
	}
	pthread_exit(0);
}

/*
 * -------------------------
 */
void checkThreadResults(int threadLineResult, int threadColumnResult) {
	if (threadColumnResult || threadLineResult || threadBlocResult) printf("Matrice incorrecte.\n");
}

/*
 * *Vérifie la validité des blocs de notre plateaux de jeu, cette fonction est manipulé par un thread.
 * *
 * *@param matrixModel  une matrice 9*9 de int qui est notre plateaux de jeu.
 * */
void T_Bloc_Verification(void *matrixModel){

	short init_i = 0,init_j = 0,init_line = 3,init_colonne = 3;

	struct MatrixModel *matrix = (struct MatrixModel*)matrixModel;

	bool bloc_check[9] ={false,false,false,false,false,false,false,false,false};
	switch(threadBlocCpt){
		case 1:
			init_i = 0;
			init_j = 3; 
			init_colonne = 6;
			init_line = 3;
			break;
		case 2:
			init_i = 0;
			init_j = 6; 
			init_colonne = 9;
			init_line = 3;
			break;
		case 3:
			init_i = 3;
			init_j = 0; 
			init_colonne = 3;
			init_line = 6;
			break;
		case 4:
			init_i = 3;
			init_j = 3; 
			init_colonne = 6;
			init_line = 6;
			break;
		case 5:
			init_i = 3;
			init_j = 6; 
			init_colonne = 9;
			init_line = 6;
			break;
		case 6:
			init_i = 6;
			init_j = 0; 
			init_colonne = 3;
			init_line = 9;
			break;
		case 7:
			init_i = 6;
			init_j = 3; 
			init_colonne = 6;
			init_line = 9;
			break;
		case 8:
			init_i = 6;
			init_j = 6; 
			init_colonne = 9;
			init_line = 9;
			break;
		default:
			break;
	}

	for(int i=init_i;i < init_line ; ++i){
		for(int j=init_j;j< init_colonne ; ++j){
			switch(matrix->table[i][j]){
				case 1:
					bloc_check[0]= true;
					break;
				case 2:
					bloc_check[1]= true;
					break;
				case 3:
					bloc_check[2]= true;
					break;
				case 4:
					bloc_check[3]= true;
					break;
				case 5:
					bloc_check[4]= true;
					break;
				case 6:
					bloc_check[5]= true;
					break;
				case 7:
					bloc_check[6]= true;
					break;
				case 8:
					bloc_check[7]= true;
					break;
				case 9:
					bloc_check[8]= true;
					break;
			}
		}
	}
		for(int k=0;k< 9;++k){
			if(bloc_check[k] != true){
				threadBlocResult = 1;
				break;
			}    
		}
	
	++threadBlocCpt;
	pthread_exit(0);
}

