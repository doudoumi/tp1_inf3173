tp1: tp1.o
	gcc -lpthread -std=c99 -o tp1 tp1.o

tp1.o: tp1.c
	gcc -lpthread -std=c99 -c tp1.c

.PHONY: clean test

clean:
	rm tp1.o tp1

test:
	./tp1 test.txt
