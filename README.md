# **Travail pratique 1**

   ## **`Description`**

   Notre projet a pour but de faire `Un vérificateur de soduku`, `Parallélisant le travail `, `la construction` et `l'exploitation` des threads dans un Systéme unix qui sera pour nous le serveur java.
   fait dans le cadre du cours ` INF3173 - Principes des systèmes d'exploitation` session `automne 2019` enseigné par **Ammar Hamad** à **L'UQAM**


   ## **`Auteur`**

   **BROURI** **Adem** <strong>`BROA23089702` && </strong> **CHIHANI**  **Amine** <strong>`CHIM28059102`</strong> && </strong> **Nom2**  **Prénom2** <strong>`code permanent 2`</strong>

   ## **`Fonctionnement`**

    
   ### 1.Se connecter au serveur ssh de l'uqam
   
  
   ### 2.Compiler le programme 
         gcc TP1.c -o TP1
    
   ![Compilation](image1.png "Compilation")
    
   ### Déroulement de la compilation
    
   ![Compilation2](image2.png "Compilation2")
    
   ### Lancement du programme version 0.1 avec output de matrice 
        ./TP1 test.txt
    
   ![Lancement1](image3.png "Lancement1")
    
    
   ## **`Contenu du projet`**

  **`TP1.c`**: Le script C contenant le code de notre projet.
  
  **`README.pdf`**: Le Manuel d'utilisation de notre projet.

   ## **`Références`**

  [**Ammar Hamad**](" https://www.apps.uqam.ca/Application/Repertoire/detail_E.aspx?P1=19870                  Répertoire des professeurs")
  
[**TANENBAUM, Andrew S. –Système d’exploitation– PERSON EDUCATION, 3RD ED.**](" https://www.renaud-bray.com/Livres_Produit.aspx?id=982287&def=Syst%C3%A8mes+d%27exploitation+3e+%C3%A9d.,TANENBAUM,+ANDREW,9782744072994                  Livre conseillé")


   ## **`Statut`**
  
  **Pas de bug notables projet complet**
   